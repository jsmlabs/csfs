#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-03
desc: Cool Storage Command Line Interface
'''

from datetime import datetime
import os
import sys

from backends.redis import Backend, IAttrName
from common.enums import ChunkserverStatus, IType

PROG_NAME = 'cscli'

#------------------------------------------------------------------------------
def parse_args_strlower(value):
    if value:
        return value.lower()


def parse_args_list(values):
    return values.split(',')


def parse_args_desc():
    serverStatusList = '|'.join(ChunkserverStatus._member_names_)
    return f'''
    {PROG_NAME} [target] [action] [item]

    {PROG_NAME} cs create
    {PROG_NAME} cs delete <id>
    {PROG_NAME} cs list
    {PROG_NAME} cs stats <id>
    {PROG_NAME} cs setstatus <id> [{serverStatusList}]

    {PROG_NAME} sc [list|create|delete]
    
    {PROG_NAME} system init [D:P]
        D - ec data parts; P - ec parity parts; ex `system init 2:1`
    {PROG_NAME} system save
    {PROG_NAME} system start
    {PROG_NAME} system stop
    '''


def parse_args(args):
    '''Parse command line'''
    import argparse

    parser = argparse.ArgumentParser(
        prog=PROG_NAME,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=parse_args_desc()
    )

    parser.add_argument('command', type=parse_args_strlower, help='system')
    parser.add_argument('options', nargs='*')
    parser.add_argument('--force', action='store_true', default=False, help='system')
    #parser.add_argument('item', type=parse_args_strlower, help='disk')
    #parser.add_argument('action', type=parse_args_strlower, help='init')

    cli = parser.parse_args(args)

    return cli


#------------------------------------------------------------------------------
def cs(options):
    if len(options) < 1:
        return error('missing action')
    action = options[0].lower()

    server_id = 0
    if len(options) > 1:
        server_id = options[1]

    if action == 'create':
        server_id = _backend.cs.create({'ip': '127.0.0.1', 'port': '9000'})
        print(f'Chunk Server ID: {server_id}')

    elif action == 'delete':
        if not server_id:
            fatal('missing server id')

        _backend.cs.delete(server_id)

    elif action == 'list':
        servers = _backend.cs.list()
        print('ID:   Status')
        print('---   ------')
        for server_id,status in servers.items():
            (ip, port) = _backend.cs.ip_port(server_id)
            status = int(status)
            print(f'{server_id}:    {str(ChunkserverStatus(status))} ({status})   {ip}:{port}')

    elif action == 'stats':
        if not server_id:
            fatal('missing server id')

        stats = _backend.cs.stats(server_id)
        status = _backend.cs.status(server_id)

        print(f'Server ID:   {server_id}')
        print(f'Updated:     {datetime.fromtimestamp(int(stats["updatetime"]))}')
        print(f'Status:      {str(ChunkserverStatus(status))} ({status})')

    elif action == 'setstatus':
        if not server_id:
            fatal('missing server id')

        if len(options) > 2:
            try:
                status = int(ChunkserverStatus[options[2]])
                _backend.cs.status(server_id, status)
                print('OK')
            except:
                serverStatusList = ', '.join(ChunkserverStatus._member_names_)
                fatal(f'Unknown status: {options[2]}\nPossible options: {serverStatusList}')

        else:
            fatal('missing arguments')

    else:
        return error('unrecognized action')


def get_inode(options):
    return _backend.inode.get_by_path(options[0])


def ls(options):
    inode = _backend.inode.get_by_path(options[0])
    if inode == None:
        return error('no such file or directory')
    entries = _backend.dir.read(inode)
    for entry in entries:
        print(entry[1])


def mkdir(options):
    '''mkdir recursively'''
    dirs = os.path.normpath(options[0]).split(os.path.sep)
    inode_p = 0
    for dir in dirs[1:]:
        inode = _backend.inode.get(inode_p, dir)
        if inode:
            inode_p = inode
            next
        inode_p = _backend.inode.create(dir, IType.Dir, inode_p)
    pass


def rmdir(options):
    inode = _backend.inode.get_by_path(options[0])
    if inode == None:
        return error('no such file or directory')
    stats = _backend.inode.read(inode)
    if stats[IAttrName.itype] != IType.Dir:
        return error('not a directory')
    if _backend.dir.count(inode) != 0:
        return error('directory not empty')
    
    _backend.inode.delete(
        stats[IAttrName.inode_p],
        os.path.basename(options[0])
    )
    

def sc(options):
    if len(options) < 1:
        return error('missing action')
    action = options[0].lower()

    if action == 'list':
        classes = _backend.sc.list()
        print(f'ID:    Format')
        print(f'---    ------')
        for id,format in classes.items():
            print(f"{id:<6} {format}")
    
    elif action == 'create':
        _backend.sc.create(options[1])

    elif action == 'delete':
        _backend.sc.delete(options[1])

    else:
        error('unrecognized action')


def stat(options):
    inode = _backend.inode.get_by_path(options[0])
    if inode != None:
        stats = _backend.inode.read(inode)
        name = os.path.basename(options[0])
        dirname = os.path.dirname(options[0])
        sc_id = stats[IAttrName.sc]
        sc = f'{_backend.sc.classes[sc_id]} ({sc_id})'
        frags = _backend.fragment.getall(inode)
        
        print('--------')
        print(f'name:    { name }')
        print(f'dir:     { dirname }')
        print(f'inode:   { inode }')
        print(f'pinode:  { stats[IAttrName.inode_p] }')
        print(f'iType:   { IType(stats[IAttrName.itype])}')
        print(f'mode:    { stats[IAttrName.mode]}')
        print(f'gid:     { stats[IAttrName.gid]}')
        print(f'uid:     { stats[IAttrName.uid]}')
        print(f'size:    { stats[IAttrName.size]}')
        print(f'ctime:   { stats[IAttrName.ctime]}')
        print(f'mtime:   { stats[IAttrName.mtime]}')
        print(f'sc:      { sc }')
        print(f'frags:   { ",".join(frags)}')
        print('--------')
    else:
        return error('no such file or directory')


def system(options):
    if options[0] == '':
        fatal('unrecognized action')

    elif options[0] == 'init':
        print('formatting file system')
        if len(options) > 1:
            _backend.format(options[1])
        else:
            _backend.format()

    elif options[0] == 'save':
        dir = _backend.save()
        print(f'db saved to {dir}')

    elif options[0] == 'start':
        _backend.start()

    elif options[0] == 'stop':
        _backend.stop()

    else:
        fatal('unrecognized action')


def touch(options):
    if not options:
        return error('missing file path')
    inode = _backend.inode.get_by_path(options[0])
    if inode != None:
        return error('file path exists')

    filename = os.path.basename(options[0])
    inode_p = _backend.inode.get_by_path( os.path.dirname(options[0]) )
    if inode_p == None:
        return error(f'dir does not exist: {os.path.dirname(options[0])}')
    _backend.inode.create(filename, IType.File, inode_p)


def unlink(options):
    inode = _backend.inode.get_by_path(options[0])
    if inode == None:
        return error('no such file or directory')
    stats = _backend.inode.read(inode)

    if _cli.force == False and stats[IAttrName.itype] != IType.File:
        return error('not a file')
    
    ## lookup parent inode (incase inode is corrupt)
    inode_p = _backend.inode.get_by_path(os.path.dirname(options[0]))
    
    _backend.inode.delete(
        inode_p,
        os.path.basename(options[0])
    )


def main():
    if _cli.command == '':
        error('unrecognized action')
    
    elif _cli.command == 'cs':
        cs(_cli.options)

    elif _cli.command == 'inode':
        print(get_inode(_cli.options))

    elif _cli.command == 'ls':
        ls(_cli.options)

    elif _cli.command == 'mkdir':
        mkdir(_cli.options)

    elif _cli.command == 'rmdir':
        rmdir(_cli.options)

    elif _cli.command == 'sc':
        sc(_cli.options)

    elif _cli.command == 'stat':
        stat(_cli.options)

    elif _cli.command == 'system':
        system(_cli.options)

    elif _cli.command == 'touch':
        touch(_cli.options)

    elif _cli.command == 'unlink':
        unlink(_cli.options)

    else:
        error('unrecognized action')


def error(msg):
    print(f'ERROR: {msg}')


def fatal(msg):
    print(f'FATAL: {msg}')
    sys.exit(1)


if __name__ == '__main__':
    _backend = Backend()
    _cli = None
    #try:
    _cli = parse_args(sys.argv[1:])
    main()
    #except:
    #    fatal('System error occured')

