#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-12
desc: Cool Storage enums
'''

from enum import IntEnum

class ChunkAction(IntEnum):
    write = 1
    read = 2
    update = 3
    delete = 4
    register = 5
    move = 6

class ChunkResponseStatus(IntEnum):
    ok = 1
    error = 2
    chunk_unavailable = 3
    chunk_not_found = 4
    chunk_crc_error = 5

class ChunkserverStatus(IntEnum):
    disabled = 0
    alive = 1
    dead = 2
    chunk_registration = 3
    maintenance = 4
    unavailable = 5
    unreachable = 6
    server_registration = 7
    client_failed_write = 8
    client_failed_read = 9
    evac = 10

    def __str__(self):
        return '%s' % self.name

class ECType(IntEnum):
    isa_l_rs_vand = 1
    liberasurecode_rs_vand = 2
    jerasure_rs_vand = 3
    jerasure_rs_cauchy = 4
    flat_xor_hd_3 = 5
    flat_xor_hd_4 = 6
    isa_l_rs_cauchy = 7
    shss = 8
    libphazr = 9

    def __str__(self):
        return '%s' % self.name

class IType(IntEnum):
    Dir = 1
    File = 2

    def __str__(self):
        return '%s' % self.name