#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-15
desc: Cool Storage chunkserver methods
'''
from gevent import socket
import orjson
import struct
from typing import Type, Tuple

from common.dataclasses import ChunkMessage, ChunkResponse, ChunkAction
from common.enums import ChunkResponseStatus


class ChunkServer():
    ''' chunk server methods
    '''
    
    def __init__(self, backend=None):
        self.backend = backend
        self._socket_buff_size = 4096


    def delete(self, server: int, inode: int, chunk: int, frag: int) -> Type[ChunkResponse]:
        return self.message_send(
            server, 
            ChunkMessage(ChunkAction.delete, inode, chunk, frag)
        )[0]


    def move(self, source: int, dest: int, inode: int, chunk: int, frag: int) -> Type[ChunkResponse]:
        return self.message_send(
            source, 
            ChunkMessage(ChunkAction.move, inode, chunk, frag)
        )[0]


    def read(self, server: int, inode: int, chunk: int, frag: int) -> Tuple[Type[ChunkResponse], bytearray]:
        return self.message_send(
            server, 
            ChunkMessage(ChunkAction.read, inode, chunk, frag)
        )


    def write(self, server: int, inode: int, chunk: int, frag: int, data: bytes) -> Type[ChunkResponse]:
        return self.message_send(
            server, 
            ChunkMessage(ChunkAction.write, inode, chunk, frag, len(data)), 
            data
        )[0]


    def data_receive(self, sock: Type[socket.socket], data_size: int) -> bytearray:
        data = bytearray()
        while len(data) < data_size:
            packet = sock.recv(self._socket_buff_size)
            if not packet:
                return None
            data.extend(packet)
        
        return data
        

    def message_send(self, server: int, message: Type[ChunkMessage], data: bytes =b'') -> Tuple[Type[ChunkResponse], bytearray]:
        (ip, port) = self.backend.cs.ip_port(server)
        try:
            ## send message
            message.data_size = len(data)
            json = orjson.dumps(message)
            length = bytearray(struct.pack("<L", len(json)))

            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((ip, int(port)))
            client.sendall(length + json + data)

            ## receive response
            response_len = (struct.unpack('<L', client.recv(4)))[0]
            response = ChunkResponse(**orjson.loads(
                client.recv(response_len)
            ))
            data = bytearray()
            if response.data_size:
                data = self.data_receive(client, response.data_size)
                
            client.close()
            return (response, data)


        except Exception as e:
            print('Server.message_send() exception: ', str(e))
            #logger.error( f'writing to chunk server [{server}:{port}] was interrupted; exception: {e}')
            pass

        finally:
            client.close()


    def message_receive(self, client: Type[socket.socket]) -> Type[ChunkMessage]:
        # message_len + message_json [+ data]
        try:
            message_len = (struct.unpack(
                '<L', 
                client.recv(4)
            ))[0]
            return ChunkMessage(**orjson.loads(
                client.recv(message_len)
            ))

        except Exception as e:
            return ChunkResponse(
                ChunkResponseStatus.error,
                f'Exception while reading socket: {str(e)}'
            )


    def response_send(self, client: Type[socket.socket], message: Type[ChunkResponse], data: bytearray =bytearray()) -> None:
        json = orjson.dumps(message)
        length = bytearray(struct.pack("<L", len(json)))
        if not data:
            data=bytearray()
        try:
            client.sendall(length + json + data)
        except Exception as e:
            print('Server.response_send() exception: ', str(e))

