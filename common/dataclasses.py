#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-12
desc: Cool Storage dataclasses
'''
from dataclasses import dataclass

from common.enums import ChunkAction, ChunkResponseStatus


@dataclass
class ChunkMessage:
    action:    ChunkAction
    inode:     int
    chunk_num: int
    frag_num:  int
    data_size: int = 0   ## how many bytes the following message is


@dataclass
class ChunkResponse:
    status:    ChunkResponseStatus
    message:   str = ''
    data_size: int = 0
    