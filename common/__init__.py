#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-12
desc: Cool Storage common methods
'''

import logging
import logging.handlers
import os
import sys
import yaml

logger = logging.getLogger()


def conf_read(path, req = [], die=True):
    '''read yaml conf file

    path : str  : full path to conf file

    req : list : list of required conf entries/keys

    die  : bool : die if conf file cannot be read; default = True

    return : obj : conf object
    '''
    missing = []
    logging.debug(path)
    if not os.path.isfile(path):
        logging.critical( f'cannot read file: [{path}]' )
        if die:
            sys.exit(1)
        return None
    with open(path, "r") as fh:
        conf = yaml.load(fh, Loader=yaml.Loader)
    
    if req:
        for r in req:
            if not r in conf:
                missing.append(r)

    if missing:
        logging.critical( f'conf file missing the following: {", ".join(missing)}' )
        if die:
            sys.exit(1)

    return conf


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


def logger_init(server_id=None, debug=False):
    global logger

    ID = '' if server_id == None else f'ID {server_id} :: '

    formatter = logging.Formatter(f'%(filename)s[%(process)s] :: {ID}%(threadName)s::%(funcName)s() :: %(message)s')
    handler = logging.handlers.SysLogHandler(address = '/dev/log')
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    if debug:
        handler.setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)
    else:
        handler.setLevel(logging.INFO)
        logger.setLevel(logging.INFO)
    logger.addHandler(handler)

def printdebug(msg):
    '''print debug message'''
    print(f'DEBUG: {msg}')

def printerror(msg):
    '''print error message'''
    print(f'ERROR: {msg}')