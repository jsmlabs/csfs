#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-12-30
desc: Cool Storage chunk methods: assemble/disable chunk fragments, and send/recieve from chunk servers
'''

from backends.redis import IAttrName
from common import printdebug, printerror
from common.enums import ChunkResponseStatus
from common.chunkserver import ChunkServer

from concurrent.futures import ThreadPoolExecutor
from pyeclib.ec_iface import ECDriver

class Chunk():
    ''' 
    Chunk methods
    '''
    
    def __init__(self, ec_d : int, ec_p : int, ec_type : str, backend):
        self._backend = backend
        self._buf_read  = {}
        self._buf_write = {}
        self._chunk_size = 134_217_728
        self._chunkserver = ChunkServer(self._backend)
        self._ec_driver = ECDriver(k=ec_d, m=ec_p, ec_type=ec_type)
        self._read_buf_max_bytes = 500_000_000
        self._sc = {}
        self._thread_pool = ThreadPoolExecutor()

        self.sc_load()


    def __del__(self):
        self._thread_pool.shutdown()


    async def append(self, inode: int, data: bytes ) -> None:
        '''
        Appending bytes to buffer. Will flush buffer (via write()) when full.
        '''
        self._buf_write[inode]["data"].extend(data)
        if len(self._buf_write[inode]["data"]) > self._chunk_size:
            await self.write(inode)


    async def delete_all(self, inode: int) -> None:
        '''
        Deletes all fragments for inode from chunkservers.

        Updates inode fragment info.
        '''
        printdebug(f'chunk.delete() {inode}')
        
        ## call this to build the server list, fragment counts, etc; 
        ## we'll clean up at the end
        await self.buffer_create(inode)
        
        ## fragment count will give us the number of servers we need
        f_cnt = self._buf_read[inode]["fragment_cnt"]

        ## pull off the first X number of servers from list
        ## servers are kept in the correct order in the list
        servers = self._buf_read[inode]["fragment_servers"][:f_cnt]
        del self._buf_read[inode]["fragment_servers"][:f_cnt]

        while servers:
            for cnt,server in enumerate(servers):
                response = self._chunkserver.delete(
                    server,
                    inode,
                    self._buf_read[inode]["chunk_num"],
                    cnt,
                )
                if response.status != ChunkResponseStatus.ok:
                    printerror(f'fragment read failed; server id: {server}; error: {response.message}')
            
            ## increment chunk num
            self._buf_read[inode]["chunk_num"] += 1

            ## get next set of servers
            servers = self._buf_read[inode]["fragment_servers"][:f_cnt]
            del self._buf_read[inode]["fragment_servers"][:f_cnt]

        self._backend.fragment.delete_all(inode)


    async def load(self, inode: int) -> None:
        '''
        Reads fragments from chunkserver, and assembles them back into chunks.
        '''
        printdebug(f'chunk.read() - inode: {inode};')
        
        f_cnt = self._buf_read[inode]["fragment_cnt"]   ## total number of fragments = data + parity
        f_req = self._buf_read[inode]["ec_d"]           ## minimum number of fragments needed to read
        
        # select set of servers to read one chunk of fragments from
        servers_all = self._buf_read[inode]["fragment_servers"][:f_cnt]
        del self._buf_read[inode]["fragment_servers"][:f_cnt]
        servers = servers_all[:f_req]
        del servers_all[:f_req]

        if not servers:
            ## chunk.read() can be called an extra time if we think 
            ##   we're going to go over buffer size but in fact we are EOF
            return

        params = []
        for cnt,server in enumerate(servers):
            params.append((server, inode, self._buf_read[inode]["chunk_num"], cnt))

        data = []
        results = self._thread_pool.map(lambda f: self._chunkserver.read(*f), params)
        for (response, d) in results:
            if response.status == ChunkResponseStatus.ok:
                data.append(bytes(d))
            else:
                printerror(f'fragment read failed; server id: {server}; error: {response.message}')

        self._buf_read[inode]["data"].extend(self._ec_driver.decode(data))
        self._buf_read[inode]["datasize"] = len(self._buf_read[inode]["data"])
        self._buf_read[inode]["chunk_num"] += 1

        if self._buf_read[inode]["datasize"] >= self._read_buf_max_bytes:
            ## this will discard older data in the buffer
            remove_size = self._buf_read[inode]["datasize"] - self._read_buf_max_bytes
            printdebug(f'buf size: {self._buf_read[inode]["datasize"]}; will flush {remove_size} bytes')
            self._buf_read[inode]["data"] = self._buf_read[inode]["data"][remove_size:]
            self._buf_read[inode]["datasize"] = len(self._buf_read[inode]["data"])
            self._buf_read[inode]["seekoffset"] += remove_size


    async def read(self, inode :int, offset :int, length :int) -> bytes:
        '''
        Read bytes from cached chunk, which was assembled by load().
        '''
        read_start = offset - self._buf_read[inode]["seekoffset"]
        read_end = read_start + length
        
        # fill buffer with one complete chunk (assemble fragments)
        if read_end > self._buf_read[inode]["datasize"]:
            await self.load(inode)
            read_start = offset - self._buf_read[inode]["seekoffset"]
            read_end = read_start + length

        data = self._buf_read[inode]["data"][read_start:read_end]
        return data


    async def size(self, inode: int) -> int:
        return self._buf_write[inode]["size"]


    async def write(self, inode: int) -> None:
        '''
        Takes cached chunk, breaks into fragments, and writes to chunkserver.
        
        Updates inode fragment info.
        '''
        printdebug(f'chunk.write() - inode: {inode}; chunk_cnt: {self._buf_write[inode]["chunk_cnt"]}')

        chunkdata = self._buf_write[inode]

        buf_size = len(chunkdata["data"])
        if not buf_size:
            printdebug('_chunk_write() nothing done')
            return

        # get servers
        servers = self._backend.cs.next_available(chunkdata["fragment_cnt"])
        
        # encode
        fragments = self._ec_driver.encode(bytes(chunkdata["data"]))
        for i,fragment in enumerate(fragments):
            response = self._chunkserver.write(
                servers[i], 
                inode, 
                chunkdata["chunk_cnt"], 
                i,
                fragment,
            )
            if response.status == ChunkResponseStatus.ok:
                printdebug("_chunk_write() SUCCESS")
                self._backend.fragment.add(inode, [servers[i]])
            else:
                printdebug("_chunk_write() FAIL")
        
        self._buf_write[inode]["size"] += buf_size
        self._buf_write[inode]["chunk_cnt"] += 1
        self._buf_write[inode]["data"] = bytearray()


    async def buffer_create(self, inode: int) -> None:
        '''
        Builds buffer of inode data, fragments, and place holder for reassembled chunk data.
        '''
        stats = self._backend.inode.read(inode)

        self._buf_write[inode] = {
            'chunk_cnt': 0,
            'data': bytearray(),
            'fragment_cnt': self._sc[stats[IAttrName.sc]]['count'],
            'size': 0,
        }

        self._buf_read[inode] = {
            'chunk_num': 0,
            'data': bytearray(),
            'datasize': 0,
            'seekoffset': 0,
            'ec_d': self._sc[stats[IAttrName.sc]]['d'],
            'ec_p':self._sc[stats[IAttrName.sc]]['p'],
            'fragment_servers': self._backend.fragment.get_all(inode),
            'fragment_cnt': self._sc[stats[IAttrName.sc]]['count'],
            'stats': stats,
        }


    async def buffer_delete(self, inode: int) -> None:
        '''
        Deletes buffer data.
        '''
        if inode in self._buf_write:
            del self._buf_write[inode]

        if inode in self._buf_read[inode]:
            del self._buf_read[inode]


    def sc_load(self):
        '''
        Cache storage class info.

        NOTE: not an async method, since it is called from __init__()
        '''
        for id,format in self._backend.sc.list(refresh=True).items():
            format_parts = format.split(':')
            
            self._sc[id] = {
                'format': format,
                'd': int(format_parts[0]),     ## data parts count
                'p': int(format_parts[1]),     ## parity parts count
            }
            self._sc[id]['count'] = self._sc[id]['d'] + self._sc[id]['p']       ## total parts count