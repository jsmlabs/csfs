# Cool Storage File System

## Descripion
**Why?** There are several issues we are trying to overcome that are not available with other open source filesystems:
- distributed data storage
- JBOD disk handling
- scalable/distributed metadata storage
- erasure coding
- ease of deployment
- future: compression
- future: encryption

While there are 

## docs / links
- [Redis Commands](https://redis.io/commands)
- [Redis mem optimize](https://docs.redis.com/latest/ri/memory-optimizations/)
- [Python redis](https://redis-py.readthedocs.io/en/stable/)
- [Windows Redis Server](https://github.com/microsoftarchive/redis/releases)
- [Python Sockets](https://pymotw.com/2/socket/tcp.html)
- [Python gevent](http://blog.pythonisito.com/2012/08/building-tcp-servers-with-gevent.html)
- [Python gevent ssl](https://github.com/veryhappythings/gevent-ssl-example)
- [pyfuse](http://www.rath.org/pyfuse3-docs/operations.html)


## install
```
apt update && apt -y install nano python3 python3-pip python3-pkgconfig liberasurecode-dev libjerasure2 isal fuse3 libfuse3-dev libffi-dev build-essential python3-dev

# note: libffi-dev - pip3 install gevent requires this package

pip install --upgrade "pip>=20.3"
pip3 install argparse PyECLib pyfuse3 redis PyYAML daemonize gevent orjson

```

## dev server
```
apt install redis

redis-server &

./cscli.py system init 3:2
./cscli.py stat /

# create multiple chunkservers
for i in {1..5}; do
  ./cscli.py cs create
  ./cscli.py cs setstatus $i alive
done
./cscli.py cs list

# start chunkserver
./cschunkserver.py start -i 1

mkidr mnt
umount mnt/; ./csmount.py mnt --debug
```


## redis data structures
**NOTES:**
- where possible, store data in LISTs composed of just integers: [memory optimize](https://redis.io/topics/memory-optimization)

*inode:* `LIST <inode> [inode_p, itype, mode, gid, uid, size, ctime, mtime]`

*dir:* `HASH <inode>:d {<inode>, name}, {<inode>, name}, n`

*inode chunks/fragments:* `LIST <inode>:c [server_id, server_id, server_id, ...]`
    Simply store server IDs of each fragment in sequential order. Based on the file's storage class, we can deduce 
    which set of servers hold fragments for a chunk. EG, with `SC 2:1`, the first set of three servers hold fragments 
    for chunk 1, the second set of three servers hold fragments for chunk 2, and so on.

*global:*
 - `g` HASH
    - `inode` : INT - last inode used; new object receives next one
    - `cs_id` : INT - last chunk server id used; new object receives next one
    - `fs_size` : INT - filesystem size in bytes
 - `g-sc` HASH
    - INT : Desc - StorageClass ID : Description
- `g-cs` HASH
    - INT : Status - Server ID : Server Status (alive, dead, maintenance, etc)
- `g-cs-<id>` HASH
    - NAME : DATA - Stat Name : Stat Data