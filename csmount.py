#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-12
desc: Cool Storage fuse mount
'''

from argparse import ArgumentParser
from collections import defaultdict
from daemonize import Daemonize
import errno
from math import ceil
import pyfuse3
from pyfuse3 import FUSEError
import os
from os import fsencode, fsdecode
import stat as stat_m
import sys
import trio

from backends.redis import Backend, IAttrName
import common
from common import logger, printdebug, printerror
from common.enums import ChunkserverStatus, IType, ECType
from common.chunk import Chunk

_NAME = 'csfs'
_backend = None
_cli_opts = None
_conf = {}
_chunk = None

class Operations(pyfuse3.Operations):
    _readdir_cache = []

    def __init__(self):
        logger.debug("")
        super().__init__()


    async def _file_info(self, inode):
        '''create FileInfo object
        
        inode : int : inode of file object

        return : obj : pyfuse3.FileInfo
        '''
        ''' NOTE:
            When fileInfo.keep_cache = True (default), and we *read* a file immediately after *creating*
            it, only the first 4096 bytes are read. I'm not sure why. This seems to solve the problem.
            aka:
                1. cp file.txt mnt/file_new.txt
                2. md5sum file.txt mnt/file_new.txt
                    the results will be different
        '''

        fileInfo = pyfuse3.FileInfo()
        fileInfo.fh = inode
        fileInfo.nonseekable = True
        fileInfo.keep_cache = False
        
        return fileInfo


    async def create(self, inode_p, name, mode, flags, ctx):
        '''create new file object

        inode_p : int : parent inode

        name : str : name of file object

        mode : int : long version of file mode

        flags : unknown

        return : tuple : (pyfuse3.FileInfo, pyfuse3.EntryAttributes)
        '''
        logger.debug(f'{inode_p} :: {name} :: {mode} :: {flags}')
        printdebug(f'create() {inode_p} :: {name} :: {mode} :: {flags}')

        inode = _backend.inode.create(name, IType.File, inode_p)
        attr = await self.getattr(inode)
        fileInfo = await self._file_info(inode)
        
        await _chunk.buffer_create(inode)
        
        return (fileInfo, attr)


    async def flush(self, inode):
        logger.debug( f'{inode}' )
        printdebug(f'flush() inode: {inode};')
        await _chunk.write(inode)
        

    async def getattr(self, inode, ctx=None):
        '''return attributes for inode

        inode  : int : inode number

        ctx : unknown

        return : pyfuse3.EntryAttributes : attributes class
        '''
        #logger.info(f'{inode}')
        #printdebug(f'getattr() {inode}')

        stats = _backend.inode.read(inode)

        if stats is None:
            raise pyfuse3.FUSEError(errno.ENOENT)
        
        entry = pyfuse3.EntryAttributes()

        if stats[IAttrName.itype] == int(IType.File):
            entry.st_mode = (stat_m.S_IFREG | stats[IAttrName.mode])
        elif stats[IAttrName.itype] == int(IType.Dir):
            entry.st_mode = (stat_m.S_IFDIR | stats[IAttrName.mode])

        entry.st_ino = inode
        entry.st_size = stats[IAttrName.size]
        entry.st_uid = stats[IAttrName.uid]
        entry.st_gid = stats[IAttrName.gid]
        entry.st_ctime_ns = stats[IAttrName.ctime] * 1e9
        entry.st_mtime_ns = stats[IAttrName.mtime] * 1e9

        entry.st_rdev = 0
        entry.st_blksize = 512
        entry.st_blocks = ((entry.st_size+entry.st_blksize-1) // entry.st_blksize)

        entry.generation = 0
        entry.entry_timeout = 300
        entry.attr_timeout = 300
        
        return entry


    async def link(self, inode, new_inode_p, new_name, ctx):
        logger.debug("")
        printdebug(f'link() ')
        raise pyfuse3.FUSEError(errno.ENOSYS)


    async def lookup(self, inode_p, name, ctx=None):
        printdebug(f'lookup() {name} {inode_p}')
        logger.debug(f' {name} {inode_p}')

        inode =_backend.inode.get(inode_p, fsdecode(name))
        if inode:
            return await self.getattr(inode)
        raise pyfuse3.FUSEError(errno.ENOENT)


    async def mkdir(self, inode_p, name, mode, ctx):
        logger.debug( f'{inode_p} {name} {mode} {ctx}' )

        inode_new = _backend.inode.create(name, IType.Dir, inode_p)
        return await self.getattr(inode_new)


    async def mknod(self, inode_p, name, mode, rdev, ctx):
        printdebug(f'mknod() ')
        logger.debug("")
        raise pyfuse3.FUSEError(errno.ENOSYS)


    async def open(self, inode, flags, ctx):
        printdebug(f'open() inode: {inode}; flags: {flags}')
        logger.debug( f': {inode} : {flags} : {ctx}' )

        fileInfo = await self._file_info(inode)

        if flags & os.O_RDONLY:
            printdebug('open for reading only')
            pass

        if flags & os.O_WRONLY:
            printdebug('open for writing only')
            pass
        
        if flags & os.O_RDWR:
            printdebug('open for reading and writing')
            pass
        
        if flags & os.O_NONBLOCK:
            printdebug('do not block on open')
            pass

        if flags & os.O_APPEND:
            printdebug('append on each write')
            pass
        
        if flags & os.O_CREAT:
            printdebug('create file if it does not exist')
            pass
        
        if flags & os.O_TRUNC:
            printdebug('truncate file')
            await _chunk.delete_all(inode)
            _backend.inode.update(inode, size=0)
            
        if flags & os.O_EXCL:
            printdebug('do not clobber existing file if O_CREAT is set')
            if flags & os.O_CREAT and _backend.inode.exists(inode):
                raise pyfuse3.FUSEError(errno.EEXIST)

        if flags & os.O_DIRECT:
            printdebug('eliminate or reduce cache effects')
            raise pyfuse3.FUSEError(errno.ENOSYS)

        if flags & os.O_NOFOLLOW:
            printdebug('do not follow symlinks')
            pass

        #if flags & os.O_FSYNC:
        #    printdebug('synchronous writes')
            ## throws error on linux: module 'os' has no attribute
        #if flags & os.O_SHLOCK:
        #    printdebug('atomically obtain a shared lock')
            ## throws error on linux: module 'os' has no attribute
        #if flags & os.O_EXLOCK:
        #    printdebug('atomically obtain an exclusive lock')
            ## throws error on linux: module 'os' has no attribute

        await _chunk.buffer_create(inode)
        
        return fileInfo


    async def opendir(self, inode, ctx):
        printdebug(f'opendir() inode: {inode}, ctx: {ctx}')
        logger.debug("")
        return inode


    async def read(self, inode, offset, length):
        #logger.debug( f'{inode} {offset} {length}')
        #printdebug(f'read() inode: {inode}; offset: {offset}; length: {length}')
        data = await _chunk.read(inode, offset, length)

        return data


    async def readdir(self, inode, offset, token):
        '''returns list of dir entries'''

        '''NOTE:
        readdir() cannot return a list of entries. Instead, it must call pyfuse3.readdir_reply().
        This in turn does one lookup, and then re-calls readdir(). Thus, we grab and store the 
        member list and take one entry from there on each readdir() call.
        '''
        printdebug(f'readdir() inode: {inode}; offset: {offset}; token {token}')
        logger.debug( f'{inode} {offset} {token}')

        if offset == 0:
            self._readdir_cache = _backend.dir.read(inode)

        entry = next(self._readdir_cache, None)     ## (inode, name)
        if entry == None:
            return False

        pyfuse3.readdir_reply( 
            token, 
            fsencode(entry[1]),
            await self.getattr( int(entry[0]) ), 
            1   # we don't use as intended; simply singals readdir() that we called ourselves
        )


    async def release(self, inode):
        printdebug(f'release() {inode}')
        logger.debug(f'{inode}')

        ## record file size
        _backend.inode.update(inode, size=await _chunk.size(inode))

        await _chunk.buffer_delete(inode)


    async def rename(self, inode_p_old, name_old, inode_p_new, name_new, flags, ctx):
        printdebug(f'rename() {inode_p_old} {name_old} {inode_p_new} {name_new}')
        logger.debug(f'{inode_p_old} {name_old} {inode_p_new} {name_new}')

        if flags != 0:
            raise FUSEError(errno.EINVAL)

        name_old = fsdecode(name_old)
        name_new = fsdecode(name_new)
        inode = _backend.inode.get(inode_p_old, name_old)
        inode_p = None

        ## did parent change?
        if inode_p_old != inode_p_new:
            _backend.inode.set_new_parent(inode_p_old, inode_p_new, inode, name_old)
            inode_p = inode_p_new
        else:
            inode_p = inode_p_old

        ## did name change?
        if name_old != name_new:
            _backend.inode.set_name(inode_p, inode, name_new)


    async def rmdir(self, inode_p, name, ctx):
        printdebug(f'rmdir() ')
        logger.debug("")

        inode = _backend.inode.get(inode_p, fsdecode(name))

        if inode:
            if _backend.dir.count(inode) > 0:
                raise pyfuse3.FUSEError(errno.ENOTEMPTY)
            else:
                _backend.inode.delete(inode_p, name)


    async def setattr(self, inode, attr, fields, fh, ctx):
        '''update attributes

        inode  : int : 

        attr   : str : attribute name

        fields : str : fields

        fh     :     : 

        ctx    :     : 

        return : pyfuse3.EntryAttributes : attributes class
        '''
        ## TODO - this whole method needs help
        printdebug(f'setattr() {inode} : {attr} : {fields} : {fh} : {ctx}')
        logger.debug( f'{inode} : {attr} : {fields} : {fh} : {ctx}')

        stats = _backend.inode.read(inode)
        if fields.update_gid:
            printdebug(f'setattr() - gid')
            stats[IAttrName.gid] = attr.st_gid

        if fields.update_mode:
            printdebug(f'setattr() - mode')
            stats[IAttrName.mode] = attr.st_mode

        if fields.update_size:
            if stats[IAttrName.size] != 0 and attr.st_size == 0:
                printdebug(f'setattr() - truncating filesize; was {stats[IAttrName.size]}, now is {attr.st_size}')
                await _chunk.delete_all(inode)
            stats[IAttrName.size] = attr.st_size

        if fields.update_uid:
            printdebug(f'setattr() - uid')
            stats[IAttrName.uid] = attr.st_uid

        ## SKIP: let update() push new mtime whenever it is touched
        #if fields.update_mtime:
        #    printdebug(f'setattr() - mtime')
        #    stats.mtime = attr.st_mtime_ns // 1000000000        
        ## SKIP: can use this as a true "create time"
        #if attr.update_ctime:
        #    printdebug(f'setattr() - ctime')

        _backend.inode.update(
            inode, 
            stats[IAttrName.mode],
            stats[IAttrName.gid],
            stats[IAttrName.uid],
            stats[IAttrName.size],
        )
        return await self.getattr(inode)


    async def statfs(self, ctx):
        printdebug(f'statfs() ')
        logger.debug("")

        fs_bytes = _backend.fs_size()
        inode = int(_backend.inode.next(False))

        ## 1 PiB
        PiB = 1024 ** 4

        stats = pyfuse3.StatvfsData()
        stats.f_bsize = 4096
        stats.f_frsize = 4096
        stats.f_blocks = PiB
        stats.f_bfree = (stats.f_blocks - ceil(fs_bytes/stats.f_bsize))
        stats.f_bavail = stats.f_bfree
        stats.f_files = 1_000_000_000
        stats.f_ffree = (stats.f_files - inode)
        stats.f_favail = stats.f_ffree
        stats.f_namemax = 4096
        return stats


    async def unlink(self, inode_p, name, ctx):
        printdebug(f'unlink() inode_p: {inode_p}; name: {name}')
        
        inode =_backend.inode.get(inode_p, fsdecode(name))
        await _chunk.delete_all(inode)
        _backend.inode.delete(inode_p, fsdecode(name))
        await _chunk.buffer_delete(inode)


    async def write(self, inode, offset, buf):
        #logger.debug(f'{inode} :: {offset}')   ##  :: {buf}
        #printdebug(f'write() {inode} :: {offset}')   ##  :: {buf})

        await _chunk.append(inode, buf)

        return len(buf)


#---------------------------------------------------------
# still need to work on these
#
    async def readlink(self, inode, ctx):
        printdebug(f'readlink() {inode}')
        logger.debug("readlink()")
        raise pyfuse3.FUSEError(errno.ENOSYS)

        path = self._inode_to_path(inode)
        try:
            target = os.readlink(path)
        except OSError as exc:
            raise FUSEError(exc.errno)
        return fsencode(target)


    async def symlink(self, inode_p, name, target, ctx):
        printdebug(f'symlink() {inode_p} {name} {target}')
        logger.debug("symlink()")
        raise pyfuse3.FUSEError(errno.ENOSYS)

        name = fsdecode(name)
        target = fsdecode(target)
        parent = self._inode_to_path(inode_p)
        path = os.path.join(parent, name)
        try:
            os.symlink(target, path)
            os.chown(path, ctx.uid, ctx.gid, follow_symlinks=False)
        except OSError as exc:
            raise FUSEError(exc.errno)
        stat = os.lstat(path)
        self._add_path(stats.st_ino, path)
        return await self.getattr(stats.st_ino)

#------------------------------------------------------------------


def parse_args(args):
    '''Parse command line'''

    parser = ArgumentParser()

    parser.add_argument('mountpoint', type=str, help='Where to mount the file system')
    parser.add_argument('--conf', default='csmount.yml', help='path to conf file')
    parser.add_argument('-D', '--daemon', action='store_true', default=False, help='run as daemon')
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debugging output')
    parser.add_argument('--debug-fuse', action='store_true', default=False, help='Enable FUSE debugging output')

    return parser.parse_args(args)


def main():
    global _chunk
    # setup EC
    ec_root = _backend.inode.read(_backend._r_inode)[IAttrName.sc]
    (ec_d, ec_p) = _backend.sc.get(ec_root).split(':')
    
    _chunk = Chunk(ec_d, ec_p, _conf['ec_type'], _backend)
    
    operations = Operations()

    logger.debug('Mounting...')
    fuse_options = set(pyfuse3.default_options)
    fuse_options.add(f'fsname={_NAME}')
    if _cli_opts.debug_fuse:
        fuse_options.add('debug')
    try:
        pyfuse3.init(operations, _cli_opts.mountpoint, fuse_options)
    except Exception as e:
        print(f"\nCannot use mounpoint: [{_cli_opts.mountpoint}]\n\nError: {e}")
        sys.exit(1)

    try:
        logger.debug('Entering main loop..')
        trio.run(pyfuse3.main)
    except:
        pyfuse3.close(unmount=False)
        raise

    logger.debug('Unmounting..')
    pyfuse3.close()


if __name__ == '__main__':
    # import cli options
    _cli_opts = parse_args(sys.argv[1:])
    common.logger_init(debug=_cli_opts.debug)
    _conf = common.conf_read(_cli_opts.conf)
    
    ## setup backend
    _backend = Backend(ip=_conf['redis_ip'], port=_conf['redis_port'])
    _backend.isformatted()  ## quits with fatal

    if _cli_opts.daemon:
        print('run as a daemon')
        daemon = Daemonize(app=f'{_NAME}', pid=f'/tmp/csmount.daemonize.{os.getpid()}', action=main)
        daemon.start()
    else:
        main()
