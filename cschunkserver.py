#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-16
desc: Cool Storage chunkserver
'''
from argparse import ArgumentParser
from gevent import socket
import os
import sys
from typing import Type, Tuple

from common.chunkserver import ChunkServer
from common.dataclasses import ChunkMessage, ChunkResponse
from common.enums import ChunkAction, ChunkResponseStatus

_chunk = ChunkServer()
_CHUNK_PATH = '/tmp/cschunkserverdata'

def c_delete(message: Type[ChunkMessage]) -> Type[ChunkResponse]:
    filepath = file_path(message)
    debug(f'ACTION: delete - {filepath}')
    try:
        os.unlink(filepath)
        return ChunkResponse(ChunkResponseStatus.ok)
    except Exception as e:
        error(f'c_read() failed: {str(e)}')
        return (ChunkResponse(ChunkResponseStatus.error, str(e)))


def c_move(message: Type[ChunkMessage]) -> Type[ChunkResponse]:
    filepath = file_path(message)
    debug(f'ACTION: move - {filepath}')
    return ChunkResponse(ChunkResponseStatus.error, 'unsupported')


def c_read(message: Type[ChunkMessage]) -> Tuple[Type[ChunkResponse], bytes]:
    filepath = file_path(message)
    debug(f'ACTION: read - {filepath}')

    if not os.path.isfile(filepath):
        return (ChunkResponse(ChunkResponseStatus.chunk_not_found), bytes())
    
    try:
        with open(filepath, 'rb', (1024*256)) as fh:
            data = fh.read()
        return (ChunkResponse(ChunkResponseStatus.ok, data_size=len(data)), data)

    except Exception as e:
        error(f'c_read() failed: {str(e)}')
        return (ChunkResponse(ChunkResponseStatus.error, str(e)), bytes())


def c_register() -> Type[ChunkResponse]:
    debug(f'ACTION: register')
    return ChunkResponse(ChunkResponseStatus.ok)


def c_write(message: Type[ChunkMessage], data: bytearray) -> Type[ChunkResponse]:
    filepath = file_path(message)
    debug(f'ACTION: write - {filepath}')
    try:
        with open(filepath, 'wb') as fh:
            fh.write(data)
        return ChunkResponse(ChunkResponseStatus.ok)

    except Exception as e:
        error(f'c_write() failed: {str(e)}')
        return ChunkResponse(ChunkResponseStatus.error, str(e))


def file_path(message: Type[ChunkMessage]) -> str:
    filename = f'{message.inode}_{message.chunk_num}_{message.frag_num}'
    subfolder = inode_to_subfolder(message.inode)
    return f'{_CHUNK_PATH}/{subfolder}/{filename}'


def inode_to_subfolder(inode, name_len=3):
    '''converts inode number to subfolder name

    inode : str : current inode number

    name_len : int : length of subfolder name to return; default 3

    return : str : subfolder name
    '''
    return str(inode)[(name_len*-1):].zfill(name_len)


def handle_connection(client, address):
    ''' A basic connection handler that applies a receiver object to each connection.
    '''
    debug(f'Client connected: {address}')

    message = _chunk.message_receive(client)
    response = None
    data = bytearray()

    debug('----------')
    debug(f'MESSAGE: {message}')

    if message.action == None:
        debug(f'ACTION: unknown')
        response = ChunkResponse(ChunkResponseStatus.error, 'action unknown')

    elif message.action == ChunkAction.delete:
        response = c_delete(message)

    elif message.action == ChunkAction.move:
        response = c_move(message)

    elif message.action == ChunkAction.read:
        response, data = c_read(message)

    elif message.action == ChunkAction.register:
        response = c_register()

    elif message.action == ChunkAction.write:
        data = _chunk.data_receive(client, message.data_size)
        response = c_write(message, data)

    else:
        debug(f'ACTION: unknown')
        response = ChunkResponse(ChunkResponseStatus.error, 'action unknown')

    _chunk.response_send(client, response, data)

    debug('----------')


def server_start(ip: str ='127.0.0.1', port: int =9000) -> None:
    # register local chunks
    c_register()

    # setup TCP server
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((ip, port))
    sock.listen(1)
    print(f'server listening on {ip}:{port}')

    # start heartbeat
    #helper.heartbeat(_ID)

    # write initial server stats (like disk size, etc)
    #helper.stats_set()

    # Wait for a connection
    while True:
        print('waiting for a connection')
        connection, client_address = sock.accept()
        handle_connection(connection, client_address)

    ## CLEANUP
    # when shutting down, mark self as maintenance mode
    #helper.stats_set( _ID, {
    #    'status' : helper.ChunkserverStatus.index('MAINTENANCE')
    #})


def server_stop(ip: str ='127.0.0.1', port: int =9000) -> None:
    print('server stop')

    #_kill_file = f'/run/{os.path.basename(__file__)}.{_ID}.kill'
    #with open(_kill_file,'w') as fh:
    #    fh.write('die')

    # making TCP connection will force while loop to check for kill file
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((ip, port))
    client.close()


#-------------------------------------
def parse_args(args):
    '''Parse command line'''
    parser = ArgumentParser()
    parser.add_argument('action', help='start | stop | register')
    parser.add_argument('--chunk_path', help='path to store chunks')
    parser.add_argument('--conf', help='path to conf file')
    parser.add_argument('-D', '--daemon', action='store_true', default=False, help='run as daemon')
    parser.add_argument('--redis_ip', help='redis server ip')
    parser.add_argument('--redis_port', default='6379', help='redis server ip')
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debugging output')
    parser.add_argument('-i', '--id', default='', help='server id')

    cli = parser.parse_args(args)
    cli.action = cli.action.lower()

    return cli


def debug(msg):
    print(msg)

def fatal(msg):
    print(f'\nFATAL: {msg}')
    sys.exit(1)

def error(msg):
    print(f'\nERROR: {msg}')
    return False


def main():
    debug(f'TEMP make {_CHUNK_PATH}')
    for i in range(0,100):
        os.makedirs(f'{_CHUNK_PATH}/{str(i).zfill(3)}', exist_ok=True)

    if 'register' == _cli.action:
        if not _cli.chunk_path:
            fatal('missing --chunk_path')
        #server_register(redis_ip = redis_ip, redis_port = redis_port, chunk_path = _cli.chunk_path)

    elif 'start' == _cli.action:
        if not _cli.id:
            fatal('missing server id')
        server_start()

    elif 'stop' == _cli.action:
        if not _cli.id:
            fatal('missing server id')
        server_stop()

    else:
        fatal(f'unrecognzied action on command line: [{_cli.action}]')


if __name__ == '__main__':
    # import cli options
    _cli = parse_args(sys.argv[1:])

    if _cli.daemon:
        print('run as a daemon')
        #daemon = Daemonize(app="CSChunkserver", pid=f'/tmp/client.daemonize.{os.getpid()}', action=main)
        #daemon.start()

    else:
        main()