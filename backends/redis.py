#!/usr/bin/env python3
'''
auth: JSM Labs
date: 2021-11-03
desc: Cool Storage backend redis helper functions
'''

from enum import IntEnum
from os import fsdecode
import os
import re
import redis
from shutil import copyfile
import subprocess
import sys
import time

from common.enums import ChunkserverStatus, IType

class Backend():
    ''' backend redis storage system
    '''
    
    def __init__(self, ip='127.0.0.1', port='6379'):
        self._r_inode = 1  ## root inode number
        self.ip = ip
        self.port = port
        self.connect()  # connect before executing code, so we can cache data from db

        self.inode = self.Inode(self)
        self.dir = self.Dir(self)
        self.sc = self.SC(self)
        self.cs = self.ChunkServer(self)
        self.fragment = self.Fragment(self)


    def connect(self, ip: str =None, port: str =None) -> None:
        '''setup database connection

        ip : str : ip address of redis server; default to what was provided to init()

        port : str : TCP port of redis server; default to what was provided to init()

        return : void
        '''
        if ip == None:
            ip = self.ip
        if port == None:
            port = self.port
        if ip == None or port == None:
            fatal('missing redis server ip/port')
        self.db = redis.Redis(host=ip, port=port, db=0, decode_responses=True)


    def format(self, ec_format: str ='2:1') -> None:
        '''format/initial database with initial data and settings
        
        ec_format : str : e.g. "2:1", "4:2", etc
        '''
        ## ensure basic dirs/files are in place
        if not os.path.isdir('/etc/redis/'):
            os.makedirs('/etc/redis/', mode=0o750)
        if not os.path.isdir('/var/lib/redis/'):
            os.makedirs('/var/lib/redis/', mode=0o750)
        if not os.path.exists('/etc/redis/redis.conf'):
            copyfile(f'{os.path.dirname(__file__)}/redis.conf', '/etc/redis/redis.conf')

        ## set needed OS configuration
        ### vm.overcommit allows redis to fork and write db to disk
        results = subprocess.run(['sysctl', 'vm.overcommit_memory'], capture_output=True)
        if results.stdout.strip() == 'vm.overcommit_memory = 0':
            with open('/etc/sysctl.conf', 'a') as fh:
                fh.write('vm.overcommit_memory = 1\n')
            subprocess.run(['sysctl', 'vm.overcommit_memory=1'])

        ### disable redis systemctl; should always be started via `cscli system start`
        subprocess.run(['systemctl', 'stop', 'redis'], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
        subprocess.run(['systemctl', 'disable', 'redis'], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
        self.start()

        ## reconnect to db
        self.connect(self.ip, self.port)

        if self.inode.exists(0):
            fatal('filesystem already formatted')

        ## setup redis server config
        self.db.config_set('save', '86400 1000')
        self.db.config_set('appendonly', 'yes')
        try:
            self.db.config_rewrite()
        except Exception as e:
            fatal(f'formatting redis filesystem failed: {str(e)}')

        ## create default storage class
        if not self.sc.create(ec_format):
            return

        ## create root inode
        # we cannot use inode.create() since there is no parent yet
        self.db.rpush(self._r_inode, self._r_inode, int(IType.Dir), 755, 0, 0, 0, int(time.time()), int(time.time()), 1)

        ## start inode count
        self.db.hset('g', 'inode', self._r_inode)

        ## set initial filesystem size
        self.backend.db.hset('g', 'fs_size', 1)

        self.save()


    def fs_size(self) -> int:
        return int(self.db.hget('g', 'fs_size'))


    def isformatted(self, stop=True):
        stat = self.inode.read(self._r_inode)
        if not stat:
            if stop:
                fatal('filesystem not formatted')
            else:
                return False
        return True


    def save(self, bg=True):
        '''save database to disk
        
        bg : bool : save process runs in background

        return : str : path to dir where db is saved to
        '''
        if bg:
            self.db.bgsave()
        else:
            self.db.save()

        data = self.db.config_get('dir')
        return data['dir']


    def start(self):
        ## disable transparent_hugepage (THP) improves performance
        ## this setting is not available via sysctl, thus needs to happen before every start
        with open('/sys/kernel/mm/transparent_hugepage/enabled', 'w') as fh:
            fh.write(f'never\n')

        subprocess.run(['/usr/bin/redis-server', '/etc/redis/redis.conf'])

        self.connect()
        for i in range(10):
            try:
                self.db.ping()
                break
            except:
                print('waiting for server to come up')
                time.sleep(1)


    def stop(self):
        self.db.shutdown(save=True)


    class Inode():
        def __init__(self, backend):
            '''inode specific methods
            
            backend : Backend() : pre-intatiated Backend object
            '''
            self.backend = backend


        def create(self, name, itype, inode_p, mode=None, gid=0, uid=0, size=0, ctime=int(time.time()), mtime=int(time.time())):
            '''
            name    : str : name of object

            itype   : IType : inode type

            inode_p : int : parent inode

            mode : int : 644, 755, etc

            gid : int : group id

            uid : int : user id

            size : int : bytes of object

            ctime : epoch : create time

            mtime : epoch : modify time

            return  : int : inode
            '''
            name = fsdecode(name)
            
            if not mode:
                mode = 755 if itype == IType.Dir else 644
            
            inode = self.next()
            # https://redis.io/topics/memory-optimization
            # lists composed of just INTs are encoded in a very memory efficient way
            stats = self.read(inode_p)
            self.backend.db.rpush(inode, inode_p, int(itype), mode, gid, uid, size, ctime, mtime, stats[IAttrName.sc])
            self.backend.db.hset(f'{inode_p}:d', inode, name)

            return inode


        def delete(self, inode_p, name):
            '''deletes inode data
            
            inode_p : int : parent inode

            name : str : name of object (aka filename/dirname)
            '''
            name = fsdecode(name)
            inode = self.get(inode_p, name)
            if inode:
                self.backend.db.delete(inode)
                self.backend.db.delete(f'{inode}:d')
                self.backend.db.delete(f'{inode}:c')
                self.backend.db.hdel(f'{inode_p}:d', inode)


        def exists(self, inode):
            '''checks if inode exists
            
            inode : int : inode to check for
            
            return : bool : True if it exists, else False
            '''
            return self.backend.db.exists(inode)


        def get(self, inode_p, name):
            '''get inode of an object by name and parent inode

            inode_p : int : parent inode

            name : str : name of object (aka filename/dirname)

            return : int : inode
            '''
            for inode_c, name_c in self.backend.db.hscan_iter(f'{inode_p}:d'):
                if name == name_c:
                    return int(inode_c)


        def get_by_path(self, path):
            '''get inode of an object by its full path

            path : str : full path to object

            return : int : inode
            '''
            path = os.path.normpath(path)
            inode = self.backend._r_inode

            if path == os.path.sep:
                return inode
            
            parts = path.split(os.path.sep)
            for name in parts[1:]:
                inode = self.get(inode, name)
            
            return None if inode == 0 else inode


        def next(self, inc : bool =True) -> int:
            '''retrieves next available inode number, and increments next inode by one

            inc : bool : increment the count automatically
            
            return : int
            '''
            if inc:
                return self.backend.db.hincrby('g', 'inode', 1)
            return self.backend.db.hget('g', 'inode')


        def read(self, inode):
            '''read inode info

            inode : int : inode to read

            return : list : inode_p, IType, mode, gid, uid, size, ctime, mtime, sc

            note: can reference each data point using IAttrName - e.g. IAttrName.mode
            '''
            data = self.backend.db.lrange(inode, 0, -1)
            # convert everything to INTs
            for i,d in enumerate(data):
                data[i] = int(d)

            return data


        def set_name(self, inode_p, inode, name):
            self.backend.db.hset(f'{inode_p}:d', inode, name)


        def set_new_parent(self, inode_p_old, inode_p_new, inode, name):
            self.backend.db.lset(inode, int(IAttrName.inode_p), inode_p_new)
            self.backend.db.hdel(f'{inode_p_old}:d', inode)
            self.backend.db.hset(f'{inode_p_new}:d', inode, name)


        def update(self, inode, mode=None, gid=None, uid=None, size=None, mtime=int(time.time())):
            data = self.read(inode)
            
            updated = False
            if mode != None:
                updated = True
                data[IAttrName.mode] = mode
            
            if gid != None:
                updated = True
                data[IAttrName.gid] = gid
            
            if uid != None:
                updated = True
                data[IAttrName.uid] = uid
            
            if size != None:
                updated = True

                ## update filesytem total size
                inode_orig_size = data[IAttrName.size]
                if inode_orig_size > 0:
                    # remove original filesize from the total
                    self.backend.db.hincrby('g', 'fs_size', (inode_orig_size*-1))
                self.backend.db.hincrby('g', 'fs_size', size)

                data[IAttrName.size] = size

            if updated:
                data[IAttrName.mtime] = mtime
                self.backend.db.delete(inode)
                self.backend.db.rpush(inode, *data)


    class Dir():
        def __init__(self, backend):
            '''dir specific methods
            
            backend : Backend() : pre-intatiated Backend object
            '''
            self.backend = backend


        def count(self, inode):
            '''return count of child entries

            inode : int : inode to lookup
            '''
            return self.backend.db.hlen(f'{inode}:d')
        

        def create(self, name, inode_p, mode=None, gid=0, uid=0, size=0, ctime=int(time.time()), mtime=int(time.time())):
            self.backend.inode.ceate(name, IType.Dir, inode_p, mode, gid, uid, size, ctime, mtime)


        def read(self, inode):
            '''read dir entries
            
            return : itr
            '''
            return self.backend.db.hscan_iter(f'{inode}:d')


    class Fragment():
        def __init__(self, backend):
            '''fragment specific methods
            
            backend : Backend() : pre-intatiated Backend object
            '''
            self.backend = backend


        def delete_all(self, inode: int) -> None:
            self.backend.db.delete(f'{inode}:c')


        def add(self, inode: int, servers: list) -> None:
            self.backend.db.rpush(f'{inode}:c', *servers)


        def get_all(self, inode: int) -> list:
            return self.backend.db.lrange(f'{inode}:c', 0, -1)


    class SC():
        def __init__(self, backend):
            '''storage class specific methods
            
            backend : Backend() : pre-intatiated Backend object
            '''
            self.backend = backend
            self.classes = {}

            self.__load_classes()


        def __load_classes(self):
            ## need to make sure keys are INTs
            try:
                for k,v in self.backend.db.hgetall('g-sc').items():
                    self.classes[int(k)] = v
            except:
                ## during system init, this will fail since db is not running yet
                pass


        def create(self, format: int) -> int:
            '''create new storage class
            
            format : str : 'D:E' format

            return : int : new storage class ID
            '''
            # need to cache storage classes in memory
            
            if not re.fullmatch('\d+:\d', format):
                return error('storage class format not correct: D:N')
            
            id = 0
            exists = False
            for i,f in self.classes.items():
                if f == format:
                    exists = True
                if int(i) > id:
                    id = int(i)
            id+=1

            if exists:
                return error(f'storage class already exists: {format}')
            
            self.backend.db.hset('g-sc', id, format)
            self.classes[id] = format

            return id


        def delete(self, id: int) -> None:
            id = int(id)
            if not self.backend.db.hexists('g-sc', id):
                return error('storage class id does not exist')
            if id == 1:
                return error('cannot delete default storage class')
            
            del self.classes[id]
            self.backend.db.hdel('g-sc', id)

        
        def get(self, id: int) -> str:
            '''return format for given storage class
            
            id : int : storage class id

            return : str : storage class format 
            '''
            if not id in self.classes:
                return error('storage class id does not exist')
            return self.classes[id]


        def list(self, refresh: bool =False) -> dict:
            '''provide list of storage classes

            refresh : bool : pull fresh list from database

            return : dict : key - class id, value - class format
            '''
            if refresh:
                self.__load_classes()
            return self.classes


    class ChunkServer():
        def __init__(self, backend):
            '''chunk server specific methods
            
            backend : Backend() : pre-intatiated Backend object
            '''
            self.backend = backend


        def __build_availability(self):
            '''builds redis sorted score set

            previous: helper.chunkserver_build_available()
            '''
            "NOTE: should use zset + score"
            pass


        def create(self, data={}):
            id = self.next_id()
            self.backend.db.hset('g-cs', id, int(ChunkserverStatus.server_registration))
            self.stats(id, data)
            
            return id


        def delete(self, id):
            self.backend.db.delete(f'g-cs-{id}')
            self.backend.db.hdel('g-cs', id)


        def list(self):
            return self.backend.db.hgetall('g-cs')


        def heartbeat(self, id):
            self.backend.db.hset(f'g-cs-{id}', 'heartbeat', int(time.time))


        def ip_port(self, id):
            return self.backend.db.hmget(f'g-cs-{id}', ['ip', 'port'])


        def isalive(self, id, seconds=10):
            heartbeat_time = self.backend.db.hget(f'g-cs-{id}', 'heartbeat')
            return True if heartbeat_time >= (time.time-seconds) else False


        def stats(self, id, data=None):
            stats = self.backend.db.hgetall(f'g-cs-{id}')
            if data != None:
                stats = {**data, **stats}     ## combines both dictionarys; values in right-most dictionary wins
                stats['updatetime'] = int(time.time())
                self.backend.db.hset(f'g-cs-{id}', mapping=stats)

            return stats


        def status(self, id: str, status: IntEnum = None) -> None:
            if status != None:
                self.backend.db.hset(f'g-cs', id, status)
            data = self.backend.db.hget(f'g-cs', id)
            data = data if data else ChunkserverStatus.unavailable
            return int(data)


        def next_id(self):
            '''retrieves next available inode number, and increments next inode by one
            
            return : int
            '''
            return self.backend.db.hincrby('g', 'cs_id', 1)


        def next_available(self, count: int) -> list:
            '''
            TODO
             - was helper.chunkserver_next_available()
             - should pull server ids from pool that self.__build_availability() maintains
             - need to provide safety, incase not enough servers are available
            '''
            return list(self.backend.db.hgetall('g-cs').keys())[0:count]


class IAttrName(IntEnum):
    inode_p = 0
    itype = 1
    mode = 2
    gid = 3
    uid = 4
    size = 5
    ctime = 6
    mtime = 7
    sc = 8
     
def fatal(msg):
    print(f'\nFATAL: {msg}')
    sys.exit(1)


def error(msg):
    print(f'\nERROR: {msg}')
    return False
    